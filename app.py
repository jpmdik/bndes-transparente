# coding: utf-8

from flask import Flask, request, Response, render_template, redirect, make_response, url_for
from flask_cors import CORS
from modelo import prever_resultado, treinar_modelo, adicionar_afirmacao

app = Flask(__name__, static_url_path='/static')

@app.route('/', methods=['GET'])
def home():
  query = request.args.get('query') if request.args.get('query') else ""
  data = query if query else -1
  if data != -1:
    data = prever_resultado([data])
    print(data)
  return render_template('index.html', query=query, data=data)

@app.route('/afirmar', methods=['GET'])
def afirmar():
  query = request.args.get('query')
  data = request.args.get('query') if request.args.get('query') else -1
  target = int(request.args.get('target')) if request.args.get('target') else -1
  if data != -1 and target != -1:
    adicionar_afirmacao(data, target)
    treinar_modelo()
    data = target
  target = 'Verdadeira' if target == 1 else 'Falsa'
  return render_template('afirmacao.html', query=query,data=data, target=target)

if __name__ == '__main__':
  treinar_modelo()
  app.run(debug=True)