import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.preprocessing import LabelEncoder
from xgboost import XGBClassifier
import numpy as np

classifier = None
vectorizer = None

def treinar_modelo():
    global vectorizer, classifier

    df = pd.read_csv('data/noticias.csv')
    x_train, y_train = df['texto'], df['fake']

    vectorizer = _obter_vectorizer()
    model = _obter_modelo()

    # Vetorizando as noticias e textos.
    tfidf = vectorizer.fit_transform(x_train)
    classifier = model.fit(tfidf, y_train)


def prever_resultado(list_notices=[]):
    x_test = pd.DataFrame({'texto': list_notices})

    # Fazendo a previsão, fake ou não.
    tfidf = vectorizer.transform(x_test['texto'])
    return classifier.predict(tfidf)


def _obter_vectorizer():
    with open('words/stopwords.txt') as file:
        stopwords = file.read()
        
    return TfidfVectorizer(stop_words=stopwords.split(), max_df=0.7)


def _obter_modelo():
    return XGBClassifier(gamma=0.1, n_jobs=-1) 

def adicionar_afirmacao(afirmacao, is_fake):
    df = pd.read_csv('data/noticias.csv') 
    df_temp = pd.DataFrame({'texto': [afirmacao], 'fake': [is_fake], 'id': df.shape[0]})
    df = df.append(df_temp, ignore_index=True, sort=False)
    df.to_csv('data/noticias.csv', index=False)
        



